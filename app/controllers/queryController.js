'use strict';

const queryModel = require('../model/queryModel');
exports.ansQueries = function(req, res) {
    // console.log(req.params.id);
    queryModel.ans(req.params.id, function(err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.status(200).send(result);
        }
    });
};
exports.queryFiletes = function(req, res) {
    queryModel.postAns(req.body, function(err, result) {
        // res.send(result);
        if (err) {
            res.send(err);
        } 
        else {
            if (result.length == 0) res.status(200).send("No Data Found");
            else res.status(200).send(result);
        }
    });
    // res.send('Hello');
};