'use strict';

const { custom, func } = require('joi');
const customerModel = require('../../model/Security/customerModel');

exports.listAllCustomers = function(req, res) {
    // res.send("hello");
    customerModel.getAllCustomers(function(err, customers) {
        if (err) {
            res.send(err);
        }
        else
        {
            console.log(customers);
            res.status(200).send(customers);
        }
    });
};

exports.create_customer = function(req, res) {
    const newCustomer = req.body;
    // validate    
    // res.send(newCustomer);
    customerModel.createCustomer(newCustomer, function(err, customer) {
        if (err) {
            logger.log("error " + err.message);
        }
        else {
            res.json(customer);
        }
    });
};