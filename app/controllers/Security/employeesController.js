'use strict';

const EmployeeModel = require('../../model/Security/employeeModel');
const utility = require('../../../libs/utility/validation');
const logger = require('../../../libs/helper/logger');
const { Helper } = require('../../../libs/helper');

logger.info("Writing from employee controller, Get Controller name");

// let userController = {};
let controllerName = Helper.getFileName(__filename, __dirname);

logger.info("Writing from the user controler, Get controller name " + controllerName);

exports.listAllEmployees = function (req, res) {
    logger.info("Retriving users list from Employee API");
    EmployeeModel.getAllEmployee(function (err, result) {
        if (err) {
            logger.info("Error Retriving employees list from User API " + err.message);
            res.send(err);
        }
        else {
            console.log('Employee List\n', result);
            res.status(200).send(result);
        }
    });
};
exports.create_employee = function (req, res) {
    logger.info("Creating New Employee form Employee API");
    const newEmployee = req.body;
    // Validate Req.Body here
    // Validate Employee Email
    EmployeeModel.createEmployee(newEmployee, function (err, employee) {
        if (err) {
            logger.log("error " + err.message);
            res.send(err);
        }
        else {
            res.json(employee);
        }
    });
    // console.log(req.body);
    // res.send('mello');
};
exports.add_salary = function (req, res) {
    const newSalary = req.body;
    EmployeeModel.addSalary(newSalary, function (err, salary) {
        if (err) {
            res.send(err);
        }
        else {
            res.status(200).send(salary);
        }
    });
};