'use strict';

const sql = require('../../../libs/dbConnect/mysqlCon');
const logger = require('../../../libs/helper/logger');

let Employee = function(employee) {
    this.id = employee.employee_id;
    this.username = employee.username;
    this.password = employee.password;
    this.email = employee.email;  
    this.join_date = employee.join_date;
    this.address = employee.address;
    this.country = employee.country;
};
Employee.createEmployee = function(newEmployee, employee)
{
    sql.query("INSERT INTO employee SET ?", newEmployee, function (err, res) {            
        if(err) {
            console.log("error: ", err);
            employee(err, null);
        }
        else {
            console.log(res.insertId);
            employee(null, res.insertId);
        }
    });    
}
Employee.getAllEmployee = function (result)
{
    logger.info("Getting Employee List");
    // var query = `CALL spGetEmployees ()`; 
    var query = 
        "SELECT e.employee_name, s.salary, e.join_date, e.employee_id, s.last_promotion_date " +
        "FROM employee e " + 
        "JOIN salary_table s " + 
        "ON s.employee_id = e.employee_id"; 

    sql.query(query, function(err, res)
    {
        if(err)
        {
            logger.info("Getting Employee List error: " + err);
            console.log("error: ", err);
            result(null, err);
        }
        else
        {    
            logger.info("Getting Employee List : " + res[0].TotalRow);
            result( null ,res);       
        }
    }); 
};
Employee.addSalary = function(newSalary, salaryCallback) {
    sql.query("INSERT INTO salary_table SET ?", newSalary, function (err, res) {            
        if(err) 
        {
            console.log("error: ", err);
            salaryCallback(err, null);
        }
        else 
        {
            console.log(res.insertId);
            salaryCallback(null, "Hello");            
        }
    });  
};
module.exports = Employee;

/*
    employee_name: Masum D Costa
    join_date: 2021-01-04
    address: Dhanmondi 15
    country: BD
*/

/*
    employee_id: 1
    last_promotion_date: 2021-01-01
    salary: 20000.003
*/
