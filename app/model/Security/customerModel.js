'use strict';
 
const { func } = require('joi');
const sql = require('../../../libs/dbConnect/mysqlCon');

let Customer = function(customer) {
    this.cust_id = customer.cust_id,
    this.cust_name = customer.cust_name,
    this.employee_id = customer.employee_id,
    this.isEmployee = customer.isEmployee,
    this.lastPurchaseDate = customer.lastPurchaseDate    
};
Customer.getAllCustomers = function(customers) {
    var query = "Select * from customer_table";
    sql.query(query, function(err, res)
    {
        if(err)
        {            
            console.log("error: ", err);
            customers(null, err);
        }
        else
        {            
            customers(null ,res);     
        }
    });
    // customers(query);
}
Customer.createCustomer = function(newCustomer, customer) {
    sql.query("INSERT INTO customer_table SET ?", newCustomer, function (err, res) {            
        if(err) {
            console.log("error: ", err);
            customer(err, null);
        }
        else {
            console.log(res.insertId);
            customer(null, res.insertId);
        }
    });
};
module.exports = Customer;

/*
cust_name: Mohibullah
isEmployee: 0
lastPurchaseDate: 2020-01-03
*/