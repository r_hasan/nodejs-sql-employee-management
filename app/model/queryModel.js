'use strict';

const sql = require('../../libs/dbConnect/mysqlCon');

let query = {};

// query.ans = function(callback) {
//     callback(null, JSON.stringify([1, 2, 3]));
// }

query.ans = function(id, callback) {
    // console.log(id);
    // var cmd = "select * from employee";
    var cmd, question;
    if (id == 1) {
        cmd = 
            "SELECT employee_name, join_date " +
            "FROM employee e " +
            "LEFT JOIN customer_table c "+
            "ON e.employee_id = c.employee_id;";
        question = "Find the employee who is also a customer";
    }
    else if (id == 2) {
        cmd = 
            "select * " +
            "from customer_table " +
            "where lastPurchaseDate is not null " +
            "and isEmployee is true;";
        question = "find the customer who is also a employee and have a purchase history";
    }
    // else if (id == 3) {
        
    // }
    console.log(cmd);
    sql.query(cmd, function(err, res) {
        if (err) {
            callback(err, null);
        }
        else {
            // res.question = question;
            console.log(res);
            callback(null, res);
        }
    });
};
query.postAns = function(queries, callback) {
    // console.log(queries);  
    var cmd, question;  
    if (queries.id == 1) {
        cmd = 
            "SELECT employee_name, join_date " +
            "FROM employee e " +
            "LEFT JOIN customer_table c "+
            "ON e.employee_id = c.employee_id;";
        question = "Find the employee who is also a customer";
    }
    else if (queries.id == 2) {
        cmd = 
            "select * " +
            "from customer_table " +
            "where lastPurchaseDate is not null " +
            "and isEmployee is true;";
        question = "find the customer who is also a employee and have a purchase history";
    }
    else if (queries.id == 3) {
        const nameStartWith = queries.name;
        const isEmployee = (queries.isEmployee == 0 ? "null" : "not null");
        // console.log(nameStartWith);
        cmd = 
            "SELECT e.employee_name, s.salary, e.join_date, e.employee_id, c.cust_id, s.last_promotion_date, c.isEmployee, c.cust_name "+
            "FROM employee e " + 
            "LEFT JOIN customer_table c  " +
            "ON e.employee_id = c.employee_id " +
            "JOIN salary_table s " +
            "ON e.employee_id = s.employee_id " +
            "WHERE e.employee_name LIKE \""+ nameStartWith + "%\" " + 
            "AND c.employee_id is " + isEmployee  + ";";
        // cmd = 
        //     `SELECT e.employee_name, s.salary, e.join_date, e.employee_id, c.cust_id, s.last_promotion_date, c.isEmployee, c.cust_name
        //     FROM employee e 
        //     LEFT JOIN customer_table c 
        //     ON e.employee_id = c.employee_id 
        //     JOIN salary_table s
        //     ON e.employee_id = s.employee_id
        //     WHERE e.employee_name LIKE \" ${nameStartWith}%\" 
        //     AND c.employee_id is ${isEmployee};`;
        console.log(cmd);
        question = "find the employee name, sal,join date address, employee id , customer id, last_promotion_date, is employee, cust_name." +
        "whose emp_name start withn ? and also a customer or not (true/false),";
    }
    else if (queries.id == 4) {
        const nameStartWith = queries.name;
        const isEmployee = (queries.isEmployee == 0 ? "null" : "not null");
        const country = queries.country;
        cmd = 
            "SELECT e.employee_name, s.salary, e.join_date, e.employee_id, c.cust_id, s.last_promotion_date, c.isEmployee, c.cust_name " +
            "FROM employee e " + 
            "JOIN customer_table c " +
            "ON e.employee_id = c.employee_id " +
            "JOIN salary_table s " +
            "ON e.employee_id = s.employee_id " +
            "WHERE e.employee_name LIKE \""+ nameStartWith + "%\" " +
            "AND c.isEmployee is "  + isEmployee + " " +
            "AND e.country LIKE \"%"+ country +"%\";";
        console.log(cmd);
        question = "find the employee name, sal,join date address, employee id , customer id, last_promotion_date, is employee, cust_name." +
        "whose emp_name start withn ? or also a customer or not (true/false), and country is ?";
    }
    else if (queries.id == 5) {
        const nameStartWith = queries.name;
        const isEmployee = (queries.isEmployee == 0 ? "null" : "not null");
        const country = queries.country;
        const lastPromotionFrom = queries.from;
        const lastPromotionTo = queries.to;
        cmd = 
            "SELECT e.employee_name, s.salary, e.join_date, e.employee_id, c.cust_id, s.last_promotion_date, c.isEmployee, c.cust_name " +
            "FROM employee e "  +
            "JOIN salary_table s " + 
            "ON s.employee_id = e.employee_id " + 
            "LEFT JOIN customer_table c " + 
            "ON c.employee_id = e.employee_id " +
            "WHERE e.employee_name LIKE \""+ nameStartWith +"%\" " +
            "AND c.isEmployee is "  + isEmployee + " " +
            "AND e.country LIKE \"%"+ country +"%\"" +
            "AND s.last_promotion_date BETWEEN '"+ lastPromotionFrom +"' AND '"+ lastPromotionTo +"';";
        question = "find the employee name, sal,join date address, employee id , customer id, last_promotion_date, is employee, cust_name. \
        whose emp_name start withn ? and also a customer or not (true/false), ( and country is ?,or promotion_date between ?)";
        console.log(cmd);
        // sql.query('SELECT e.employee_name, s.salary, e.join_date, e.employee_id, c.cust_id, s.last_promotion_date, c.isEmployee, c.cust_name FROM employee e \
        //             JOIN salary_table s ON s.employee_id = e.employee_id LEFT JOIN customer_table c ON c.employee_id = e.employee_id \
        //             WHERE e.employee_name LIKE ? AND c.isEmployee= ? AND e.country LIKE ? AND s.last_promotion_date BETWEEN ? AND ?', [nameStartWith + '%', queries.isEmployee, country + '%', lastPromotionFrom, lastPromotionTo], function(err, res) {
        //     if (err) {
        //         callback(err, null);
        //     }
        //     else {
        //         // res.question = question;
        //         console.log(res);
        //         callback(null, res);
        //     }
        // });
    }
    sql.query(cmd, function(err, res) {
        if (err) {
            callback(err, null);
        }
        else {
            // res.question = question;
            console.log(res.length);                        
            callback(null, res);
        }
    });
}

module.exports = query;


// id:5
// isEmployee:0
// name: M
// country:BD
// from:2021-01-01
// to:2021-02-05