const employeeRoute = require('./api/employee/route');
const customerRoute = require('./api/customer/route');
const queryRoute    = require('./api/queryRoute');
module.exports.API = 
{
  employeeRoute,
  customerRoute,
  queryRoute,
}