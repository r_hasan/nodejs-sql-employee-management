'use strict';

const queryController = require('../../controllers/queryController');

const queryRoute = function(app) {
    // app.get('/query/:id', (req, res)=>{
    //     // console.log(req.params.id);
    //     res.send('Hello');
    // });
    app.get('/query/:id', [
        queryController.ansQueries
    ]);
    app.post('/query', 
        queryController.queryFiletes
    );
};

module.exports = {queryRoute};