'use strcit';

const EmployeesController = require('../../../controllers/Security/employeesController');

const employeeRoute = function (app) {
    app.get('/employees', [    
        EmployeesController.listAllEmployees
    ]);
    app.post('/employees',
        EmployeesController.create_employee
    );
    app.post('/employees/salary', [
        EmployeesController.add_salary
    ]);
};

module.exports = {employeeRoute};