'use strict'

const { func } = require('joi');
const customerController = require('../../../controllers/Security/customerController');

const customerRoute = function(app) {
    app.get('/customers', [
        customerController.listAllCustomers
    ]);
    app.post('/customers', [
        customerController.create_customer
    ]);
};

module.exports = {customerRoute};