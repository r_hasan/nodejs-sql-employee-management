Employee Table,
employee_id,
employee_name,
join_date,
adress,
country

Salary Table,
sal_id,
employee_id,
Salary,
last_promotion_date,

Customer Table,
cust_name,
cust_id,
EmployeeID,
IsEmployee,
LastPurchaseDate,

** new:
Find the employee who is also a customer,
<!--  
    1. SELECT * FROM tl_assignment.customer_table where employee_id is not null;
    2. SELECT employee_name, join_date
        FROM employee e
        LEFT JOIN customer_table c
        ON e.employee_id = c.employee_id;
 -->
find the customer who is also a employee and have a purchase history,
<!-- 
    select * 
    from customer_table
    where lastPurchaseDate is not null
    and employee_id is not null;

    select * 
    from customer_table
    where lastPurchaseDate is not null
    and isEmployee is true;
 -->

find the employee name, sal,join date address, employee id , customer id, last_promotion_date, is employee, cust_name.
whose emp_name start withn ? and also a customer or not (true/false),
<!-- 
SELECT e.employee_name, s.salary, e.join_date, e.employee_id, c.cust_id, s.last_promotion_date, c.isEmployee, c.cust_name
FROM employee e
LEFT JOIN customer_table c 
ON e.employee_id = c.employee_id
JOIN salary_table s
ON e.employee_id = s.employee_id
WHERE e.employee_name LIKE " M%";
 -->
find the employee name, sal,join date address, employee id , customer id, last_promotion_date, is employee, cust_name.
whose emp_name start withn ? or also a customer or not (true/false), and country is ?,

<!-- 


SELECT e.employee_name, s.salary, e.join_date, e.employee_id, c.cust_id, s.last_promotion_date, c.isEmployee, c.cust_name
 
WHERE e.employee_name LIKE "%%"
AND c.isEmployee is true or false
AND e.country LIKE "%%";
 -->

find the employee name, sal,join date address, employee id , customer id, last_promotion_date, is employee, cust_name.
whose emp_name start withn ? and also a customer or not (true/false), ( and country is ?,or promotion_date between ?)

<!-- 
SELECT e.employee_name, s.salary, e.join_date, e.employee_id, c.cust_id, s.last_promotion_date, c.isEmployee, c.cust_name
FROM employee e
JOIN salary_table s
ON s.employee_id = e.employee_id
LEFT JOIN customer_table c 
ON c.employee_id = e.employee_id
WHERE e.employee_name LIKE "%%"
AND c.isEmployee is true
AND e.country LIKE "%%"
AND s.last_promotion_date BETWEEN '2021-01-01' AND '2021-01-05';
 -->
If data not found then show a logical error.