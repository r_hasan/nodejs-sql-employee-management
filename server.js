const http = require('http');
const express = require('express');
const config = require('./common/config/env.config.js');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.get('/', (req, res) => {
  res.send('Hello');  
});

app.set('case sensitive routing', true);
var Routes = require('./app/routes/route'); //importing route

Routes.API.employeeRoute.employeeRoute(app);
Routes.API.customerRoute.customerRoute(app);
Routes.API.queryRoute.queryRoute(app);

const port = config.port || 8000;
http.createServer(app).listen(port, function() {
  console.log('app listening at port http://localhost:%s', port);
});